$(function() {
(function($) {
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
    $.fn.attrchange = function(callback) {
        if (MutationObserver) {
            var options = {
                subtree: false,
                attributes: true
            };
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(e) {
                    callback.call(e.target, e.attributeName);
                    
                });
            });
            return this.each(function() {
                observer.observe(this, options);
            });
        }
    }
})(jQuery);
});

function randomIntFromInterval(min,max){
   return Math.floor(Math.random()*(max-min+1)+min);
}


$( document ).ready(function() {


    

  
  var slideix =jQuery(".slide"),
  underLineContent = jQuery(".under-line-content");
  
    var cnt = slideix.find('.under-line').length,
    cn = slideix.find('.under-line');

    cn.each(function( i ) {    
        var it = $(this).attr('data-iteration');
        if (it == null) {
            it = "infinite";
        };
        $(this).append('<svg class="under-line-anim" viewBox="0 0 1 1" width="100%" height="2px"><path data-it="'+it+'" class="under-line-anim-path path-line" id="pather-'+i+'" d="M-130,0 L130,0" /></svg>');
    });

    pl = slideix.find('.path-line');    
    pl.each(function( i ) {    
        var delay = ((i+1)*randomIntFromInterval(1,12));
        var duration = randomIntFromInterval(4,10);
        var iteration = $(this).attr('data-it');
        if (iteration == null) {
            iteration = "infinite";
        };
       $(this).css('animation', 'dash '+duration+'s ease-out '+delay+'s '+iteration+'');
    });

    $('.under-line-content').attrchange(function(attrName) {
      if(attrName=='class'){
               pl.each(function( i ) {    
            var delay2 = ((i+1)*randomIntFromInterval(1,12));
            var duration2 = randomIntFromInterval(4,10);
            var iteration2 = $(this).attr('data-it');
            if (iteration2 == null) {
                iteration2 = "infinite";
            };
            $(this).attr("style", "");
           $(this).css('animation', 'dash '+duration2+'s ease-out '+delay2+'s '+iteration2+'');
           $(this).toggle().toggle();
        });
      }

    });
///
setTimeout(function(){ 
    console.clear();
    console.log('');                
    console.log('%c Made by medialift.nl', 'padding: 11px 8px 11px 5px; color:#fff; background-color:#292929; line-height:25px');
    console.log('%c Do you want website?', 'padding: 11px 8px 11px 5px; color:#fff; background-color:#444; line-height:25px');              
    console.log('%c Just contact me. joost@medialift.nl', 'padding: 11px 8px 11px 5px; color:#fff; background-color:#444; line-height:25px');              
    console.log('');
}, 2000);



});


